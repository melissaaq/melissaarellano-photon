package mx.exazero.myapplication.repository

import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import mx.exazero.myapplication.model.School
import mx.exazero.myapplication.repository.dto.SchoolResponse
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class SchoolRepositoryImplTest {
    private val apiMock = mockk<SchoolApi>()
    private val repository = SchoolRepositoryImpl(schoolApi = apiMock)

    @Before
    fun setUp() {
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @Test
    fun `Given success API, when getSchoolsList(), then returns domain list`() = runTest{
        // Given
        coEvery { apiMock.getSchools() } returns listOf(
            SchoolResponse(
                dbn = "test-dbn",
                schoolName = "test school",
                overviewParagraph = "overview test"
            ),
            mockk(relaxed = true)
        )

        // When
        val result = repository.getSchoolsList()

        // Then
        assert(result.size == 2)
        with(result.first()){
            assert(this is School)
            assert(dbn == "test-dbn")
            assert(schoolName == "test school")
            assert(overview == "overview test")
        }
    }

    @Test
    fun `Given failure API, when getSchoolsList(), then returns empty`() = runTest{
        // Given
        coEvery { apiMock.getSchools() } throws Exception("test network error")

        // When
        val result = repository.getSchoolsList()

        // Then
        assert(result.size == 0)
    }
}