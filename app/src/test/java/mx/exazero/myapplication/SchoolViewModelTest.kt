package mx.exazero.myapplication

import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import mx.exazero.myapplication.repository.SchoolRepository
import org.junit.Before
import org.junit.Test


class SchoolViewModelTest(){
    private val schoolRepositoryMock = mockk<SchoolRepository>()
    private val viewModel = SchoolViewModel(
        repository = schoolRepositoryMock
    )

    @Before
    fun setup(){
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @Test
    fun `Given repository success, when fetchSchoolData, then returns list`() = runTest {
        // Given
        coEvery { schoolRepositoryMock.getSchoolsList() } returns listOf(
            mockk(relaxed = true),
            mockk(relaxed = true),
            mockk(relaxed = true),
        )

        // When
        viewModel.fetchSchoolData()

        // Then
        val result = viewModel.schoolList.toList()
        assert(
            result.size == 3
        )
    }

    @Test
    fun `Given repository failure, when fetchSchoolData, then returns empty list`() = runTest {
        // Given
        coEvery { schoolRepositoryMock.getSchoolsList() } returns emptyList()

        // When
        viewModel.fetchSchoolData()

        // Then
        val result = viewModel.schoolList.toList()
        assert(result.size == 0)
    }
}