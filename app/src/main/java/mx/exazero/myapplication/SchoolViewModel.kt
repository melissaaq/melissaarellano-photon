package mx.exazero.myapplication

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import mx.exazero.myapplication.model.School
import mx.exazero.myapplication.repository.SchoolRepository
import javax.inject.Inject

@HiltViewModel
class SchoolViewModel @Inject constructor(
    private val repository: SchoolRepository
): ViewModel() {
    val schoolList = SnapshotStateList<School>()
    private val _selectedSchool: MutableState<School?> = mutableStateOf(null)
    val selectedSchool: State<School?>
        get() = _selectedSchool

    fun fetchSchoolData(){
        viewModelScope.launch {
            val newList = repository.getSchoolsList()
            schoolList.clear()
            schoolList.addAll(newList)
        }
    }

    fun fetchSchoolByDbn(dbn: String){
        viewModelScope.launch {
            _selectedSchool.value = repository.getSchoolsList().find { it.dbn == dbn }
        }

    }
}