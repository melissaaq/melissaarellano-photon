package mx.exazero.myapplication.model

data class School(
    val dbn: String,
    val schoolName: String,
    val overview: String,
)