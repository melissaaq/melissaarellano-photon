package mx.exazero.myapplication

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import dagger.hilt.android.AndroidEntryPoint
import mx.exazero.myapplication.model.School
import mx.exazero.myapplication.ui.theme.MyApplicationTheme

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<SchoolViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.fetchSchoolData()
        setContent {
            MyApplicationTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val controller = rememberNavController()
                    NavHost(navController = controller, startDestination = "home"){
                        composable("home"){
                            HomeScreen(
                                viewModel = hiltViewModel(),
                                controller = controller
                            )
                        }
                        composable("details/{schoolDbn}", arguments = listOf(navArgument("schoolDbn"){ type = NavType.StringType})){
                            DetailsScreen(
                                controller = controller,
                                viewModel = hiltViewModel(),
                                idSchool = it.arguments?.getString("schoolDbn") ?: ""
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun HomeScreen(
    controller: NavController,
    viewModel: SchoolViewModel,
) {
    LaunchedEffect(key1 = true){
        viewModel.fetchSchoolData()
    }
    val list = viewModel.schoolList
    NycSchoolList(
        modifier = Modifier.fillMaxSize(),
        schools = list,
        onSchoolClicked = {
            controller.navigate("details/${it.dbn}")
        }
    )
}

@Composable
fun DetailsScreen(
    idSchool: String,
    controller: NavController,
    viewModel: SchoolViewModel,
) {
    LaunchedEffect(key1 = true){
        viewModel.fetchSchoolByDbn(idSchool)
    }
    val school by remember {
        viewModel.selectedSchool
    }
    Column(
        modifier = Modifier.fillMaxSize()
    ) {
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = school?.dbn?:"",
                fontSize = 12.sp
            )
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.CenterHorizontally),
                text = school?.schoolName?:"",
                fontSize = 16.sp
            )
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 8.dp),
                text = school?.overview?:"",
                fontSize = 12.sp
            )
    }
}

@Composable
fun NycSchoolList(
    modifier: Modifier = Modifier,
    schools: List<School>,
    onSchoolClicked: (School) -> Unit
){
    LazyColumn(modifier = modifier){
        items(schools){
            SchoolItem(
                modifier = Modifier.padding(8.dp),
                school = it,
                onSchoolClicked = onSchoolClicked
            )
        }
    }
}

@Composable
fun SchoolItem(
    modifier: Modifier = Modifier,
    school: School,
    onSchoolClicked: (School) -> Unit
){
    //TODO enhance UI
    var isExpanded by remember {
        mutableStateOf(false)
    }
    Card(modifier = modifier.clickable {
        // isExpanded = !isExpanded
        onSchoolClicked(school)
    }) {
        Column(
            modifier = Modifier.padding(8.dp)
        ) {
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = school.dbn,
                fontSize = 12.sp
            )
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = school.schoolName,
                fontSize = 16.sp
            )

            if(isExpanded){
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 4.dp),
                    text = school.overview,
                    fontSize = 12.sp
                )
            }
        }

    }
}

/*
@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    MyApplicationTheme {
        NycSchoolList(
            schools = listOf(School("123","test","overview"))
        )
    }
}

@Preview(showBackground = true)
@Composable
fun DetailsScreenPreview(){
    MyApplicationTheme {
        DetailsScreen(school = School(
            "12345",
            "Test School Name",
            "overview paragraph"
        ))
    }
}*/
