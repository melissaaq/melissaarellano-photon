package mx.exazero.myapplication.mapper

import mx.exazero.myapplication.model.School
import mx.exazero.myapplication.repository.dto.SchoolResponse

fun SchoolResponse.toSchoolDomain() = School(
    dbn = this.dbn,
    schoolName = this.schoolName,
    overview = this.overviewParagraph
)