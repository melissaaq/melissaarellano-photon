package mx.exazero.myapplication.repository

import mx.exazero.myapplication.repository.dto.SchoolResponse
import retrofit2.http.GET

interface SchoolApi {
    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchools(): List<SchoolResponse>
}