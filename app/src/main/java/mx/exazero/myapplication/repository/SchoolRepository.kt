package mx.exazero.myapplication.repository

import mx.exazero.myapplication.model.School

interface SchoolRepository {
    suspend fun getSchoolsList(): List<School>
}