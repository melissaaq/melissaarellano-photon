package mx.exazero.myapplication.repository.dto

import com.google.gson.annotations.SerializedName

data class SchoolResponse(
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("overview_paragraph")
    val overviewParagraph: String,
)
