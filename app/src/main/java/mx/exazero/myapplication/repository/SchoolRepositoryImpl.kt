package mx.exazero.myapplication.repository

import mx.exazero.myapplication.mapper.toSchoolDomain
import mx.exazero.myapplication.model.School
import java.lang.Exception
import javax.inject.Inject

class SchoolRepositoryImpl @Inject constructor(
    private val schoolApi: SchoolApi
): SchoolRepository {
    override suspend fun getSchoolsList(): List<School> {
        return try {
            schoolApi.getSchools().map { it.toSchoolDomain() }
        } catch (e: Exception){
            // Handle network issues
            emptyList()
        }
    }
}