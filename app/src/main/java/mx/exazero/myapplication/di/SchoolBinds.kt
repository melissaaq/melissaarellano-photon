package mx.exazero.myapplication.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import mx.exazero.myapplication.repository.SchoolRepository
import mx.exazero.myapplication.repository.SchoolRepositoryImpl

@Module
@InstallIn(SingletonComponent::class)
interface SchoolBinds {
    @Binds
    fun bindSchoolRepository(repositoryImpl: SchoolRepositoryImpl): SchoolRepository
}